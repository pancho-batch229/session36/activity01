const Task = require("../models/task.js");

// funciton
module.exports.getAllTasks = () => {
	return Task.find({}).then(results => {
		return results;
	});
}

// it has a parameter so it must be used directly
module.exports.createTasks = (requestBody) => {
	let newTask = new Task ({
		name: requestBody.name
	});

	return newTask.save().then((task, err) => {
		if(err) {
			console.log(err);
			return false;
		} else {
			return task;
		}
	});
}

module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, error) => {
		if(error){
			console.log(error);
			return false;
		} else {
			return removedTask;
		}
	});
}

module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		}

		// reassigned name
		result.name = newContent.name;

		return result.save().then((updatedTask, error) => {

			if(error){
				console.log(error);
			} else {
				return updatedTask;
			}

		});
	});
}


module.exports.getSpecifiedTask = (taskId) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		} else {
			return result;
		}
	});
}


module.exports.completeTask = (taskId) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
		} 

		result.status = "complete";

		return result.save().then((updatedTask, error) => {
			if(error){
				console.log(error);
				return false;
			} else {
				return updatedTask;
			}
		})
	});
}


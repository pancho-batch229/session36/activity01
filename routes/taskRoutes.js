// Separation of concern - splitting up codes for better code base or somethin'

const express = require("express");
const router = express.Router();
const taskControllers = require("../controllers/taskControllers.js")
 
/*

	[SECTION] - Routes
		The routes are responsible for defining the URIs that our client accesses and the corresponding controller functions that will be used when a route is accessed
		They invoke the controller functions from the controller files
		All the business logic is done in the controller

*/

// ROute to get all tasks
// parent route to avoid confusion for the system
router.get("/", (req, res) => {
	taskControllers.getAllTasks().then(resultFromController => res.send(resultFromController));
});


//  Creating a new task
router.post("/", (req, res) => {
	taskControllers.createTasks(req.body).then(resultFromController => res.send(resultFromController));
});


// Delete tasks 
router.delete("/:id", (req, res) => {
	taskControllers.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
});

// Update task
router.put("/:id", (req, res) => {
	taskControllers.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
});

// Get all specific tasks
router.get("/:id", (req, res) => {
	taskControllers.getSpecifiedTask(req.params.id).then(resultFromController => res.send(resultFromController));
});

//  Update status to "complete" 
router.put("/:id/complete", (req, res) => {
	taskControllers.completeTask(req.params.id).then(resultFromController => res.send(resultFromController));
});

module.exports = router;
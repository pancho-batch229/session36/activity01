const mongoose = require("mongoose");


// blooprint
const taskSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Task name required"]
	},
	status: {
		type: String,
		default: "pending"
	}
});

// module
// modules and schemas must be together
// exports this module
module.exports = mongoose.model("Task", taskSchema);
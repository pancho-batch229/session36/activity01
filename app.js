// Setup the modules/dependencies

const express = require("express");
const mongoose = require("mongoose");
const taskRoutes  = require("./routes/taskRoutes.js")


// Server Setup

const app = express();
const port = 5002;

app.use(express.json());
app.use(express.urlencoded({extended:true}));


// Add the tasks route
// Allows the tasks 
app.use("/tasks", taskRoutes);
app.use("/users", taskRoutes);


// Database Connection (MongoDB)

mongoose.connect("mongodb+srv://admin:admin1234@cluster0.fhytgag.mongodb.net/b229_todo?retryWrites=true&w=majority",{
	useNewUrlParser:true,
	useUnifiedTopology:true
});

// Server Listening
app.listen(port, () => console.log(`Now listening to ${port}.`));

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("Connected to cloud database.")); /*similar to listen function*/
